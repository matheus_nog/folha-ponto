var demoFormMasks = function() {

    function plugins() {
        $.mask.definitions['~'] = "[+-]";
        $('#zip_code').mask("99999-999");
        $('#zip_codeUser').mask("99999-999");
        $("#phone").mask("(99) 9999-9999?9");
        $("#date").mask("99/99/9999");
        $("#phoneUser").mask("(99) 9999-9999?9");
        $(".phoneUser").mask("(99) 9999-9999?9");

        $("#phoneExt").mask("(999) 999-9999? x99999");
        $("#zip-code").mask("99999-999");
        $("#zip-codeUser").mask("99999-999");
        $("#zip-codeReal").mask("99999-999");

        $("#iphone").mask("+33 999 999 999");
        $("#document_federal").mask("999.999.999-99");
        $("#document").mask("999.999.999-99");
        $("#document_federalUser").mask("999.999.999-99");
        $("#tin").mask("99-9999999");
        $("#ssn").mask("999-99-9999");
        $("#product").mask("a*-999-a999", {
            placeholder: " "
        });
        $("#eyescript").mask("~9.99 ~9.99 999");
        $("#po").mask("PO: aaa-999-***");
        $("#pct").mask("99%");
        $("#phoneAutoclearFalse").mask("(999) 999-9999", {
            autoclear: false
        });
        $("#phoneExtAutoclearFalse").mask("(999) 999-9999? x99999", {
            autoclear: false
        });

        $("input").blur(function() {
            // $("#info").html("Unmasked value: " + $(this).mask());
        }).dblclick(function() {
            $(this).unmask();
        });
    }

    return {
        init: function() {
            plugins();
        }
    };
}();

$(function() {
    "use strict";
    demoFormMasks.init();
});