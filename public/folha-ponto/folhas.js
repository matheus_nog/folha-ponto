class Folhas {
    save() {
        $(".parsley-form").parsley().validate()
        var id = $('#id').val()

        var empregado = $('#select_empregado').val();
        var mes_ano = $('#datepicker').val();
        var href = $("#href").val()

        var type_req = id ? 'PUT' : 'POST'
        $.ajax({
            type: type_req,
            url: '/api/empregados/gerar-folha',
            dataType: 'json',
            data: {
                'empregado': empregado,
                'mes_ano': mes_ano,
            },
            success: function (data) {
                console.log(data);
                $('#tables').empty();

                data.map(z => {
                    var table = '';
                    if (z.path) {
                        table += '<div class="row">';
                        table += '<div class="col-md-12">';
                        table += '<a class="btn btn-primary" href="/storage/pdf/' + z.path + '">Baixar PDF</a>';
                        table += '</div>';
                        table += '</div>';
                        $('#tables').append(table);
                    }
                })

                data.map(z => {
                    var table = '';

                    table += '<div class="row">';
                    table += '<div class="col-md-4">';
                    table += '<p><b>Nome do empregado: ' + z.empregado.nome + '</b></p>';
                    table += '</div>';
                    table += '<div class="col-md-4">';
                    table += '<p><b>CTPS nº e série: ' + z.empregado.ctps + '</b></p>';
                    table += '</div>';
                    table += '</div>';

                    table += '<div class="table-responsive">';
                    table += '<table class="table table-bordered no-m">';
                    table += '<thead>' +
                        '<tr>' +
                        '<td>Dia</td>' +
                        '<td>Entrada</td>' +
                        '<td>Saída almoço</td>' +
                        '<td>Retorno almoço</td>' +
                        '<td>Saída</td>' +
                        '<td>Extras</td>' +
                        '<td>Atrasos</td>' +
                        '<td>Saldo</td>' +
                        '</tr>' +
                        '</thead>';
                    table += '<tbody id="">';
                    const linhas = z.dias.length;
                    z.dias.map((u, i) => {
                        console.log(u.dia);
                        if (u.domingo == true || u.feriado == true) {
                            table += "<tr>";
                            table += "<td>" + u.dia + "</td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td>" + (u.domingo ? 'Domingo' : 'Feriado') + "</td>";
                        } else {
                            table += "<tr>";
                            table += "<td>" + u.dia + "</td>";
                            table += "<td>" + u.horario_entrada + "</td>";
                            table += "<td>" + (!u.sabado ? u.horario_saida_almoco : '') + "</td>";
                            table += "<td>" + (!u.sabado ? u.horario_retorno_almoco : '') + "</td>";
                            table += "<td>" + u.horario_saida + "</td>";
                            table += "<td>" + u.extra + "</td>";
                            table += "<td>" + u.atraso + "</td>";
                            table += "<td>" + (u.extra - u.atraso) + "</td>";

                            if (u.sabado == false) {
                                table += "<td></td>";
                            }
                        }

                        if (u.sabado == true) {
                            table += "<td>Sábado</td>";
                        }
                        table += "</tr>";

                        if (linhas === i + 1) {
                            table += "<tr>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td></td>";
                            table += "<td>Total</td>";
                            table += "<td>" + u.total_extra + "</td>";
                            table += "<td>" + u.total_atraso + "</td>";
                            table += "<td>" + (u.total_extra - u.total_atraso) + "</td>";
                            table += "<td></td>";
                            table += "</tr>";
                        }
                    })
                    table += '</tbody>';
                    table += '</table>';
                    table += '</div>';
                    table += '</br>';

                    $('#tables').append(table);
                })

                const rowData = data.length;
                // $('#table-body').empty();
                data.map((u, i) => {
                    var table = '';
                    table += "<tr>";
                    table += "<td>" + u.dia + "</td>";
                    table += "<td>" + u.horario_entrada + "</td>";
                    table += "<td>" + (!u.sabado ? u.horario_saida_almoco : '') + "</td>";
                    table += "<td>" + (!u.sabado ? u.horario_retorno_almoco : '') + "</td>";
                    table += "<td>" + u.horario_saida + "</td>";
                    table += "<td>" + u.extra + "</td>";
                    table += "<td>" + u.atraso + "</td>";
                    table += "<td>" + (u.extra - u.atraso) + "</td>";
                    if (u.sabado == true) {
                        table += "<td>Sábado</td>";
                    } else if (u.domingo == true || u.feriado == true) {
                        table = "<tr>";
                        table += "<td>" + u.dia + "</td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td>" + (u.domingo ? 'Domingo' : 'Feriado') + "</td>";
                    } else {
                        table += "<td></td>";
                    }
                    table += "</tr>";

                    if (rowData === i + 1) {
                        table += "<tr>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td></td>";
                        table += "<td>Total</td>";
                        table += "<td>" + u.total_extra + "</td>";
                        table += "<td>" + u.total_atraso + "</td>";
                        table += "<td>" + (u.total_extra - u.total_atraso) + "</td>";
                        table += "<td></td>";
                        table += "</tr>";
                    }
                    // $('#table-body').append(table);
                })

                toastr.success("Folha gerada com sucesso!");
            },
            error: function (xhr, status, error) {
                var err = JSON.parse(xhr.responseText);

                if (err.errors != null) {
                    Object.values(err.errors).forEach(element => {
                        toastr.error(element);
                    });
                } else {
                    toastr.error(err.message);
                }
            }
        });
    }

    loadFuncionarios() {
        $.ajax({
            type: "GET",
            url: '/api/empregados',
            dataType: 'json',
            success: function (data) {
                data.data.map(u => {
                    var option;
                    option = "<option value='" + u.id + "'>" + u.nome + "</option>";
                    $('#select_empregado').append(option);
                })
            },
            error: function () {
                toastr.error("Erro ao realizar requisição")
            }
        })
    }
}

var folhas = new Folhas()