class Empregados {
    save() {
        $(".parsley-form").parsley().validate()
        var id = $('#id').val()

        var empregador = $('#empregador').val();
        var cpf = $('#document').val();
        var endereco = $('#address').val();
        var nome = $("#nome").val()
        var ctps = $("#ctps").val()
        var admissao = $("#admissao").val();
        var funcao = $("#funcao").val()
        var entrada_semana = $("#entrada_semana").val()
        var saida_almoco_semana = $("#saida_almoco_semana").val()
        var retorno_almoco_semana = $('#retorno_almoco_semana').val()
        var saida_semana = $("#saida_semana").val();
        var entrada_sabado = $("#entrada_sabado").val()
        var saida_almoco_sabado = $("#saida_almoco_sabado").val()
        var retorno_almoco_sabado = $("#retorno_almoco_sabado").val()
        var saida_sabado = $("#saida_sabado").val()
        var href = $("#href").val()

        var type_req = id ? 'PUT' : 'POST'
        $.ajax({
            type: type_req,
            url: '/api/empregados' + (id ? '/' + id : ''),
            dataType: 'json',
            data: {
                'empregador': empregador,
                'cpf': cpf,
                'endereco': endereco,
                'nome': nome,
                'ctps': ctps,
                'admissao': admissao,
                'funcao': funcao,
                'entrada_semana': entrada_semana,
                'saida_almoco_semana': saida_almoco_semana,
                'retorno_almoco_semana': retorno_almoco_semana,
                'saida_semana': saida_semana,
                'entrada_sabado': entrada_sabado,
                'saida_almoco_sabado': saida_almoco_sabado,
                'retorno_almoco_sabado': retorno_almoco_sabado,
                'saida_sabado': saida_sabado,
            },
            success: function (data) {
                type_req == 'PUT' ? toastr.success("Empregado editado com sucesso!") : toastr.success("Empregado cadastrado com sucesso!");
                window.location.href = href;
            },
            error: function (xhr, status, error) {
                var err = JSON.parse(xhr.responseText);

                if (err.errors != null) {
                    Object.values(err.errors).forEach(element => {
                        toastr.error(element);
                    });
                } else {
                    toastr.error(err.message);
                }
            }
        });
    }

    get(id = null) {
        var real_state_id = $("#real_state_id").val() ? $("#real_state_id").val() : "";
        var paginate = $('#numberPages').val() ? $('#numberPages').val() : "";
        var tenant = $('#tenant').val() ? $('#tenant').val() : "";
        var insurance_broker = $("#insurance_brokerFilter").val() ? $('#insurance_brokerFilter').val() : "";
        var status = $("#statusFilter").val() ? $('#statusFilter').val() : "";
        var select_leases = "x";
        var filters = { status: status, select_leases: select_leases, tenant: tenant, insurance_broker: insurance_broker, real_state_id: real_state_id, paginate: paginate }
        var url1 = '/api/empregados';
        var url2 = id;
        if (id != 'null') {

            $.ajax({
                type: "GET",
                url: id == null ? url1 : url2,
                headers: {
                    filters: JSON.stringify(filters),
                },
                dataType: 'json',

                success: function (data) {
                    data.links.map(u => {
                        var numbers = '<li class="' + (u.url ? "" : "disabled") + ' page-item ' + (u.active ? "active" : "") + '"><a class="page-link" onClick="realStates.get(\'' + u.url + '\'); " data-value="' + u.url + '" href="#">' + u.label.toString().replace('Previous', 'Anterior').replace('Next', 'Próximo') + '</a></li>';
                        $('.pagination').append(numbers);
                    })
                    data.data.map(u => {
                        var $table = ""
                        $table += "<tr>"
                        $table += "<td>" + u.id + "</td>";
                        $table += "<td>" + u.nome + "</td>";
                        $table += "<td>" + u.ctps + "</td>";
                        $table += "<td>" + u.funcao + "</td>";                        
                        $table += "<td>" + "<a href='/empregados/cadastro/" + u.id + "'>" + "<input class='btn btn-primary' type='button' value='Editar' />" + " </a>";
                        $table += "<input class='btn btn-danger deleted_att' id='deleted_add' type='button' data-toggle='modal' data-target='#exampleModal' value='Remover' onClick='setDelete(" + u.id + ")' /></td>";

                        $('#table-body').append($table);

                    });

                },
                error: function () {
                    toastr.error("Erro ao realizar a requisição")
                }
            })
        }
    }

    load() {
        var id = $("#id").val()
        if (!id) {
            return
        }

        // ocultando pessoa física / jurídica
        $('#states').hide();

        $.ajax({
            type: "GET",
            url: '/api/empregados/' + id,
            dataType: 'json',
            success: function (data) {
                console.log(data);

                $("#empregador").val(data.empregador);
                $("#document").val(data.cpf);
                $("#address").val(data.endereco);
                $("#nome").val(data.nome);
                $("#ctps").val(data.ctps);
                $("#admissao").val(data.admissao);
                $("#funcao").val(data.funcao);
                $("#entrada_semana").val(data.horario_entrada_semana);
                $("#saida_almoco_semana").val(data.horario_saida_almoco_semana);
                $("#retorno_almoco_semana").val(data.horario_retorno_almoco_semana	);
                $("#saida_semana").val(data.	horario_saida_semana);
                $("#entrada_sabado").val(data.horario_entrada_sabado);
                $("#saida_almoco_sabado").val(data.horario_saida_almoco_sabado);
                $("#retorno_almoco_sabado").val(data.horario_retorno_almoco_sabado);
                $("#saida_sabado").val(data.horario_saida_sabado);
                
            },
            error: function () {
                toastr.error("Erro ao realizar a requisição");
            }
        });
    }

    del() {        
        $.ajax({
            type: "DELETE",
            url: '/api/empregados/' + idDelete,
            dataType: 'json',
            success: function (data) {
                empregados.get();
                $("#table-body").empty();
                $('#exampleModal').modal('hide');
                toastr.success("Empregado deletado com sucesso!");
            },
            error: function () {
                toastr.error("Erro ao realizar a requisição");
            }
        });
    }
}

var empregados = new Empregados()