<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>FOLHA DE PONTO INDIVIDUAL DE TRABALHO</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <style>
        table {
            border-collapse: collapse;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table-bordered {
            border: black 1px solid;
        }

        .text-center {
            text-align: center;
        }

        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .border-none {
            border: none !important;
            border-right: 1px solid black;
        }

        td {
            font-size: 12px;
        }

        body {
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .page_break {
            page-break-before: always;
        }

        .column {
            float: left;
            width: 40%;
        }

        .column2 {
            float: left;
            width: 60%;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>
</head>

<body>

    @foreach($todos as $folha)
    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="">FOLHA DE PONTO INDIVIDUAL DE TRABALHO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2">EMPREGADOR: {{ $folha['empregado']['empregador'] }}</td>
                <td>CPF Nº</td>
                <td>{{ $folha['empregado']['cpf'] }}</td>
            </tr>
            <tr>
                <td colspan="4">ENDEREÇO: {{ $folha['empregado']['endereco'] }}</td>
            </tr>
            <tr>
                <td colspan="2">EMPREGADO(A): {{ $folha['empregado']['nome'] }}</td>
                <td colspan="2">
                    <table class="border-none" style="width: 100%">
                        <tr class="border-none">
                            <td class="border-none text-center">
                                <b>CTPS Nº E SÉRIE:</b>
                            </td>
                            <td class="border-none text-center">
                                {{ $folha['empregado']['ctps'] }}
                            </td>
                            <td class="border-none text-center">
                                <b>ADMISSÃO:</b>
                            </td>
                            <td class="border-none text-center" style="border-right: none !important;">
                                {{ $folha['empregado']['admissao'] }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="4"><b>FUNÇÃO: {{ $folha['empregado']['funcao'] }}</b></td>
            </tr>

            <tr>
                <td colspan="2"><b>HORÁRIO DE TRABALHO DE SEG. A SEXTA FEIRA:</b></td>
                <td colspan="2"><b>HORÁRIOS AOS SÁBADOS:</b></td>
            </tr>

            <tr>
                <td colspan="2">De {{ $folha['empregado']['horario_entrada_semana'] }} as {{ $folha['empregado']['horario_saida_semana'] }}</td>
                <td colspan="2">De {{ $folha['empregado']['horario_entrada_sabado'] }} as {{ $folha['empregado']['horario_saida_sabado'] }}</td>
            </tr>

            <tr>
                <td class="text-center">INTERVALO P/ ALMOÇO</td>
                <td>Seg a Sexta: 01 hora</td>
                <td style="font-size: 11px !important;"><b>DESCANSO SEMANAL: </b> DOMINGO</td>
                <td width="100"><b>MÊS/ANO: {{ str_replace("-", "/", $folha['mes_ano']) }}</b></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table">
        <thead>
            <tr>
                <td class="text-center"><b>DIAS</b></td>
                <td class="text-center"><b>ENTRADA/MANHÃ</b></td>
                <td class="text-center"><b>ALMOÇO SAÍDA</b></td>
                <td class="text-center"><b>ALMOÇO RETORNO</b></td>
                <td class="text-center"><b>SAÍDA/TARDE</b></td>
                <td class="text-center"><b>EXTRAS</b></td>
                <td class="text-center"><b>ATRASOS</b></td>
                <td class="text-center"><b>ASSINATURA</b></td>
            </tr>
        </thead>

        <tbody>
            @foreach($folha['dias'] as $dia)
            <!-- <p>{{$dia['dia']}}</p> -->
            <tr>
                <td class="text-center">{{$dia['dia']}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text-center">@if($dia['sabado']) SÁBADO @elseif($dia['domingo']) DOMINGO @elseif($dia['feriado']) FERIADO @endif</td>
            </tr>

            @if($loop->last)
            <tr>
                <td colspan="5" class="text-center" style="font-size: 10px;">TOTAIS DE HORAS EXTRAS A RECEBER E HORAS A DESCONTAR</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>

    <br>
    <br>

    <div class="row">
        <div class="column2">
            <!-- RESUMO GERAL -->
            <table class="table">
                <tr class="text-center">
                    <td colspan="4"><b>RESUMO GERAL</b></td>
                </tr>
                <tr>
                    <td width="50" class="text-center">+</td>
                    <td>H. EXTRAS</td>
                    <td width="50" class="text-center">HS</td>
                    <td width="50"> </td>
                </tr>
                <tr>
                    <td class="text-center">(-)</td>
                    <td>FALTAS INJUSTIFICADAS</td>
                    <td class="text-center">DIA</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-center">(-)</td>
                    <td>DESCONTAR DSR</td>
                    <td class="text-center">DIA</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-center">(-)</td>
                    <td>FALTAS / ATRASOS</td>
                    <td class="text-center">HS</td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="column" style="text-align: center;">
            <p style="border-top: solid black 1px; margin-left: 10px; margin-right: 10px;">VISTO SUPERVISÃO</p>
            <br>
            <p style="border-top: solid black 1px; margin-left: 10px; margin-right: 10px;">VISTO FUNCIONÁRIO</p>
        </div>
    </div>

    <div class="page_break"></div>

    <!-- com os dados preenchidos -->
    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="">FOLHA DE PONTO INDIVIDUAL DE TRABALHO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2"><b>EMPREGADOR: {{ $folha['empregado']['empregador'] }}</b></td>
                <td class="text-center"><b>CPF Nº</b></td>
                <td><b>{{ $folha['empregado']['cpf'] }}</b></td>
            </tr>
            <tr>
                <td colspan="4"><b>ENDEREÇO: {{ $folha['empregado']['endereco'] }}</b></td>
            </tr>
            <tr>
                <td colspan="2"><b>EMPREGADO(A): </b>{{ $folha['empregado']['nome'] }}</td>
                <td colspan="2">
                    <table class="border-none" style="width: 100%">
                        <tr class="border-none">
                            <td class="border-none text-center">
                                CTPS Nº E SÉRIE:
                            </td>
                            <td class="border-none text-center">
                                {{ $folha['empregado']['ctps'] }}
                            </td>
                            <td class="border-none text-center">
                                ADMISSÃO:
                            </td>
                            <td class="border-none text-center" style="border-right: none !important;">
                                {{ $folha['empregado']['admissao'] }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="4"><b>FUNÇÃO: </b>{{ $folha['empregado']['funcao'] }}</td>
            </tr>

            <tr>
                <td colspan="2"><b>HORÁRIO DE TRABALHO DE SEG. A SEXTA FEIRA:</b></td>
                <td colspan="2"><b>HORÁRIOS AOS SÁBADOS:</b></td>
            </tr>

            <tr>
                <td colspan="2">De {{ $folha['empregado']['horario_entrada_semana'] }} as {{ $folha['empregado']['horario_saida_semana'] }}</td>
                <td colspan="2">De {{ $folha['empregado']['horario_entrada_sabado'] }} as {{ $folha['empregado']['horario_saida_sabado'] }}</td>
            </tr>

            <tr>
                <td class="text-center">INTERVALO P/ ALMOÇO</td>
                <td>Seg a Sexta: 01 hora</td>
                <td style="font-size: 11px !important;"><b>DESCANSO SEMANAL:</b> DOMINGO</td>
                <td width="100"><b>MÊS/ANO: {{ str_replace("-", "/", $folha['mes_ano']) }}</b></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table">
        <thead>
            <tr>
                <td class="text-center"><b>DIAS</b></td>
                <td class="text-center"><b>ENTRADA/MANHÃ</b></td>
                <td class="text-center"><b>ALMOÇO SAÍDA</b></td>
                <td class="text-center"><b>ALMOÇO RETORNO</b></td>
                <td class="text-center"><b>SAÍDA/TARDE</b></td>
                <td class="text-center"><b>EXTRAS</b></td>
                <td class="text-center"><b>ATRASOS</b></td>
                <td class="text-center"><b>ASSINATURA</b></td>
            </tr>
        </thead>

        <tbody>
            @foreach($folha['dias'] as $dia)
            <!-- <p>{{$dia['dia']}}</p> -->
            <tr>
                <td class="text-center">{{$dia['dia']}}</td>
                @if($dia['domingo'] || $dia['feriado'])
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                @else
                <td class="text-center">{{$dia['horario_entrada']}}</td>
                <td class="text-center">{{$dia['horario_saida_almoco']}}</td>
                <td class="text-center">{{$dia['horario_retorno_almoco']}}</td>
                <td class="text-center">{{$dia['horario_saida']}}</td>
                <td class="text-center">{{$dia['extra']}}</td>
                <td class="text-center">{{$dia['atraso']}}</td>
                @endif
                <td class="text-center">@if($dia['sabado']) SÁBADO @elseif($dia['domingo']) DOMINGO @elseif($dia['feriado']) FERIADO @endif</td>
            </tr>

            @if($loop->last)
            <tr>
                <td colspan="5" class="text-center" style="font-size: 10px;">TOTAIS DE HORAS EXTRAS A RECEBER E HORAS A DESCONTAR</td>
                <td class="text-center">{{$dia['total_extra']}}</td>
                <td class="text-center">{{$dia['total_atraso']}}</td>
                <td class="text-center">{{ $dia['total_extra'] - $dia['total_atraso'] }}</td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>

    <br>
    <br>

    <div class="row">
        <div class="column2">
            <!-- RESUMO GERAL -->
            <table class="table">
                <tr class="text-center">
                    <td colspan="4"><b>RESUMO GERAL</b></td>
                </tr>
                <tr>
                    <td class="text-center" width="50">+</td>
                    <td>H. EXTRAS</td>
                    <td class="text-center" width="50">HS</td>
                    <td class="text-center" width="50"> </td>
                </tr>
                <tr>
                    <td class="text-center">(-)</td>
                    <td>FALTAS INJUSTIFICADAS</td>
                    <td class="text-center">DIA</td>
                    <td class="text-center"></td>
                </tr>
                <tr>
                    <td class="text-center">(-)</td>
                    <td>DESCONTAR DSR</td>
                    <td class="text-center">DIA</td>
                    <td class="text-center"></td>
                </tr>
                <tr>
                    <td class="text-center">(-)</td>
                    <td>FALTAS / ATRASOS</td>
                    <td class="text-center">HS</td>
                    <td class="text-center"></td>
                </tr>
            </table>
        </div>
        <div class="column" style="text-align: center;">
            <p style="border-top: solid black 1px; margin-left: 10px; margin-right: 10px;">VISTO SUPERVISÃO</p>
            <br>
            <p style="border-top: solid black 1px; margin-left: 10px; margin-right: 10px;">VISTO FUNCIONÁRIO</p>
        </div>
    </div>

    <div class="page_break"></div>
    @endforeach

</body>

</html>