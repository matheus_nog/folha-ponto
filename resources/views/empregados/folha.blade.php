@extends('layout.layout')

@section('content')
<section class="panel">
    <header class="panel-heading no-b pb0">
        <h4><b>Gerar folha de ponto</b></h4>
        <hr>
    </header>
    <div class="panel-body">
        <form role="form" class="parsley-form" data-parsley-type-message="Formato inválido" data-parsley-equalto-message="Os campos não são iguais" data-parsley-required-message="Esse campo é obrigatório" data-part data-parsley-validate>
            <div class="row">
                <!-- <div class="col-md-4">
                    <label for="">Selecione um empregado</label>
                    <select name="empregado" id="select_empregado" class="form-control" placeholder="Imobiliárias">
                        <option value="">Selecione um empregado</option>
                    </select>
                </div> -->
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Mês/Ano *</label>
                        <div>
                            <input type="text" class="form-control" name="datepicker" id="datepicker" placeholder="Mês/Ano" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""></label>
                        <input id="btn_save" type="button" value="GERAR" class="btn btn-success btn-block">
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <!-- <div class="table-responsive">
                        <table class="table table-hover no-m">
                            <thead>
                                <tr>
                                    <td>Dia</td>
                                    <td>Entrada</td>
                                    <td>Saída almoço</td>
                                    <td>Retorno almoço</td>
                                    <td>Saída</td>
                                    <td>Extras</td>
                                    <td>Atrasos</td>
                                    <td>Saldo</td>
                                </tr>
                            </thead>

                            <tbody id="table-body">

                            </tbody>
                        </table>
                    </div> -->
                    <div id="tables"></div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="button" value="VOLTAR" class="btn btn-default btn-block" onClick="javascript: window.history.back()">
                </div>
            </div>
        </div>
    </div>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.2/locales/bootstrap-datepicker.pt-BR.min.js"></script>

    <script type="text/javascript" src="/folha-ponto/folhas.js"></script>

    <script>
        $(function() {
            folhas.loadFuncionarios();
            $("#datepicker").datepicker({
                format: "mm-yyyy",
                language: 'pt-BR',
                locale: 'pt-BR',
                startView: "months",
                minViewMode: "months",
            });

            $('#btn_save').click(function() {
                folhas.save();
            })

        })
    </script>

</section>

@stop