@extends('layout.layout')

@section('content')
<section class="panel">
    <header class="panel-heading no-b pb0">
        <h4><b>Dados do empregado</b></h4>
        <hr>
    </header>
    <div class="panel-body">
        <form role="form" class="parsley-form" data-parsley-type-message="Formato inválido" data-parsley-equalto-message="Os campos não são iguais" data-parsley-required-message="Esse campo é obrigatório" data-part data-parsley-validate>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Empregador *</label>
                        <div>
                            <input maxlength="80" type="text" class="form-control" name="empregador" id="empregador" data-parsley-required="true" data-parsley-trigger="change" placeholder="Empregador">
                            <input type="hidden" name="href" id="href" value="/empregados">
                            <input type="hidden" name="id" id="id" value="{{ $id }}">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>CPF *</label>
                        <div>
                            <input maxlength="80" type="text" class="form-control" id="document" name="document" data-parsley-required="true" data-parsley-trigger="change" placeholder="Documento">
                        </div>
                    </div>
                </div>

                <div style="clear:both">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Endereço</label>
                            <div>
                                <input maxlength="80" type="text" class="form-control" id="address" name="address" data-parsley-required="false" data-parsley-trigger="change" placeholder="Rua">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Empregado *</label>
                            <div>
                                <input maxlength="80" type="text" class="form-control" name="nome" id="nome" data-parsley-required="true" data-parsley-trigger="change" placeholder="Empregado">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>CTPS Nº e Série</label>
                            <div>
                                <input maxlength="80" type="text" class="form-control" name="ctps" id="ctps" data-parsley-required="true" data-parsley-trigger="change" placeholder="CTPS">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Admissão</label>
                            <div>
                                <input maxlength="80" type="date" class="form-control" name="admissao" id="admissao" data-parsley-required="true" data-parsley-trigger="change" placeholder="Admissão">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Função</label>
                            <div>
                                <input maxlength="80" type="text" class="form-control" name="funcao" id="funcao" data-parsley-required="true" data-parsley-trigger="change" placeholder="Função">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label>Horário de trabalho de segunda a sexta:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Entrada</label>
                            <div>
                                <input maxlength="80" type="time" class="form-control" name="entrada_semana" id="entrada_semana" data-parsley-required="true" data-parsley-trigger="change" value="09:00">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Saída para almoço</label>
                            <div>
                                <input maxlength="80" type="time" class="form-control" name="saida_almoco_semana" id="saida_almoco_semana" data-parsley-required="true" data-parsley-trigger="change" value="12:00">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Retorno almoço</label>
                            <div>
                                <input maxlength="80" type="time" class="form-control" name="retorno_almoco_semana" id="retorno_almoco_semana" data-parsley-required="true" data-parsley-trigger="change" value="13:00">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Saída</label>
                            <div>
                                <input maxlength="80" type="time" class="form-control" name="saida_semana" id="saida_semana" data-parsley-required="true" data-parsley-trigger="change" value="17:00">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label>Horário aos sábados:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Entrada</label>
                            <div>
                                <input maxlength="80" type="time" class="form-control" name="entrada_sabado" id="entrada_sabado" data-parsley-required="true" data-parsley-trigger="change" value="09:00">
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Saída</label>
                            <div>
                                <input maxlength="80" type="time" class="form-control" name="saida_sabado" id="saida_sabado" data-parsley-required="true" data-parsley-trigger="change" value="15:00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="button" value="VOLTAR" class="btn btn-default btn-block btn-lg" onClick="javascript: window.history.back()">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input id="btn_save" type="button" value="SALVAR" class="btn btn-success btn-block btn-lg">
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/folha-ponto/empregados.js"></script>

    <script>
        $(function() {
            empregados.load();
            
            $('#btn_save').click(function() {
                empregados.save();
            })

        })
    </script>

</section>

@stop