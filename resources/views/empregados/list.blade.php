@extends('layout.layout')

@section('content')
<section class="panel">

    <header class="panel-heading">
        <h4><b>Lista de empregados</b></h4>
    </header>
    <div class="panel-heading no-b">

        <div class="row">
            <div class="col-md-3 form-group">
                <input type="text" name="nameFilters" id="nameFilters" class="form-control" placeholder="Nome">
            </div>

            <div class="col-md-3 form-group">
                <input type="text" name="documentFilters" id="documentFilters" class="form-control" placeholder="CTPS">
            </div>
            <div class="col-md-3 form-group">
                <input type="text" name="functionFilters" id="functionFilters" class="form-control" placeholder="Função">
            </div>
            <div class="col-md-1 form-group no-p">
                <button type="button" class="btn btn-primary" id="btn_search"><span class="ti-search"></span></button>
                <button type="button" class="btn btn-danger" id="btn_clear"><span class="ti-na"></span> </button>
            </div>
        </div>

        <div class="panel-body">
            <div>Exibir
                <select name="" id="mySelect" class="">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select> itens por página
            </div>
        </div>

        <div class="panel-body">

            <div class="table-responsive">
                <table class="table table-hover no-m">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>CTPS</th>
                            <th>Função</th>
                        </tr>
                    </thead>

                    <tbody id="table-body">

                    </tbody>
                </table>
            </div>
        </div>
        <nav aria-label="Page navigation example" class="text-center">
            <div class="pagination">
            </div>
        </nav>
    </div>
    <a class="exit-offscreen"></a>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Remover funcionário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p class="modal_question">Tem certeza que deseja remover esse funcionário?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>

                <input type="button" id="btn_delete" class="btn btn-danger" value="Remover">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/folha-ponto/empregados.js"></script>
<script>
    $(function() {
        empregados.get()

        $("#btn_delete").click(function() {
            empregados.del();
        });

        $('#numberPages').on('change', function() {
            $('#table-body').empty();
            $('.pagination').empty();
            realStates.get();
        });

        $('#btn_search').click(function() {
            $('#table-body').empty();
            $('.pagination').empty();
            realStates.get();
        })
        $('#send_button').click(function() {
            insuranceCompanyBrokerRealStates.save();
        })
        $('#btn_clear').click(function() {
            $('#table-body').empty();
            $('.pagination').empty();
            realStates.clear();
        })

    })
</script>
@stop