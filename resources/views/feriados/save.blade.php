@extends('layout.layout')

@section('content')
<section class="panel">
    <header class="panel-heading no-b pb0">
        <h4><b>Feriado</b></h4>
        <hr>
    </header>
    <div class="panel-body">
        <form role="form" class="parsley-form" data-parsley-type-message="Formato inválido" data-parsley-equalto-message="Os campos não são iguais" data-parsley-required-message="Esse campo é obrigatório" data-part data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="feriados">Digite em cada linha a data de cada feriado no seguinte formato: 01/01/2021</label>
                        <textarea class="form-control" id="feriados" rows="10"></textarea>
                    </div>
                </div>
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label>Data</label>
                        <div>
                            <input maxlength="80" type="date" class="form-control" name="data" id="data" data-parsley-required="true" data-parsley-trigger="change" placeholder="Data">
                        </div>
                    </div>
                </div> -->
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="button" value="VOLTAR" class="btn btn-default btn-block btn-lg" onClick="javascript: window.history.back()">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input id="btn_save" type="button" value="SALVAR" class="btn btn-success btn-block btn-lg">
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/folha-ponto/empregados.js"></script>

    <script>
        $(function() {
            load();

            function load() {
                $.ajax({
                    type: "GET",
                    url: '/api/feriados/',
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $("#feriados").val(data.nome);
                    },
                    error: function() {
                        toastr.error("Erro ao realizar a requisição");
                    }
                });
            }

            $('#btn_save').click(function() {

                $(".parsley-form").parsley().validate()
                var id = $('#id').val()
                var feriados = $('#feriados').val();
                var href = $("#href").val()

                var type_req = id ? 'PUT' : 'POST'
                $.ajax({
                    type: type_req,
                    url: '/api/feriados',
                    dataType: 'json',
                    data: {
                        'feriados': feriados,
                    },
                    success: function(data) {
                        toastr.success("Feriados salvos com sucesso!");
                        // window.location.href = href;
                    },
                    error: function(xhr, status, error) {
                        var err = JSON.parse(xhr.responseText);

                        if (err.errors != null) {
                            Object.values(err.errors).forEach(element => {
                                toastr.error(element);
                            });
                        } else {
                            toastr.error(err.message);
                        }
                    }
                });
            })

        })
    </script>

</section>

@stop