<!doctype html>
<html class="no-js" lang="">

<head>
    <!-- meta -->

    <meta charset="utf-8">
    <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <!-- /meta -->

    <title>Folha de ponto</title>
    <!-- page level plugin styles -->
    <link rel="stylesheet" href="/layout/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- /page level plugin styles -->

    <!-- core styles -->
    <link rel="stylesheet" href="/layout/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout/css/font-awesome.css">
    <link rel="stylesheet" href="/layout/css/themify-icons.css">
    <link rel="stylesheet" href="/layout/css/animate.min.css">
    <!-- /core styles -->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('template-admin/app-assets/css/vendors.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('template-admin/app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template-admin/app-assets/css/core/colors/palette-gradient.css') }}">
    <!-- template styles -->
    <link rel="stylesheet" href="/layout/css/skins/palette.css" id="skin">
    <link rel="stylesheet" href="/layout/css/fonts/font.css">
    <link rel="stylesheet" href="/layout/css/main.css">
    <!-- page level plugin styles -->
    <link rel="stylesheet" href="/layout/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="/layout/plugins/toastr/toastr.min.css">
    <!-- /page level plugin styles -->

    <!-- core styles -->
    <link rel="stylesheet" href="/layout/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout/css/font-awesome.css">
    <link rel="stylesheet" href="/layout/css/themify-icons.css">
    <link rel="stylesheet" href="/layout/css/animate.min.css">
    <!-- /core styles -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


    <link rel="stylesheet" href="/layout/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="/layout/plugins/datatables/jquery.dataTables.min.css">

    <!-- template styles -->
    <link rel="stylesheet" href="/layout/css/skins/palette.css" id="skin">
    <link rel="stylesheet" href="/layout/css/fonts/font.css">
    <link rel="stylesheet" href="/layout/css/main.css">

    <link rel="stylesheet" href="/layout/plugins/icheck/skins/all.css">
    <script src="/layout/plugins/modernizr.js"></script>
    <script src="/layout/plugins/jquery-1.11.1.min.js"></script>
</head>

<!-- body -->

<body>

    <div class="app">
        <!-- top header -->
        <header class="header header-fixed navbar">

            <div class="brand">
                <!-- toggle offscreen menu -->
                <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
                <!-- /toggle offscreen menu -->

                <!-- logo -->
                <a href="/dashboard" class="navbar-brand text-center">
                    <img src="/img/inloc-branco.png" alt="">
                </a>
                <!-- /logo -->
            </div>

            <ul class="nav navbar-nav">
                <li class="hidden-xs">
                    <!-- toggle small menu -->
                    <a href="javascript:;" class="toggle-sidebar">
                        <i class="ti-menu"></i>
                    </a>
                    <!-- /toggle small menu -->
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                <li class="off-right">
                    <a href="javascript:;" data-toggle="dropdown">
                        <img src="/layout/img/faceless.jpg" class="header-avatar img-circle" alt="user" title="user">
                        
                        <i class="ti-angle-down ti-caret hidden-xs"></i>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight">
                        <li>
                            
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}"> Sair </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            @include('layout.menu')
            <!-- /sidebar menu -->

            <!-- main content -->
            <section class="main-content">

                <!-- content wrapper -->
                <div class="content-wrap">

                    <!-- inner content wrapper -->
                    <div class="wrapper">
                        @yield('content')
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>

    <!-- core scripts -->
    <script src="/layout/bootstrap/js/bootstrap.js"></script>
    <script src="/layout/plugins/jquery.slimscroll.min.js"></script>
    <script src="/layout/plugins/jquery.easing.min.js"></script>
    <script src="/layout/plugins/appear/jquery.appear.js"></script>
    <script src="/layout/plugins/jquery.placeholder.js"></script>
    <!-- /core scripts -->

    <!-- page level scripts -->


    <script src="/layout/plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="/layout/plugins/parsley.min.js"></script>
    <script src="/layout/plugins/jquery.inputmask.min.js"></script>
    <script src="/layout/plugins/toastr/toastr.min.js"></script>
    <script src="/layout/plugins/switchery/switchery.min.js"></script>
    <script src="/layout/plugins/icheck/icheck.js"></script>

    <script src="/layout/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/layout/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/layout/plugins/jquery.sparkline.js"></script>
    <script src="/layout/plugins/flot/jquery.flot.js"></script>
    <script src="/layout/plugins/flot/jquery.flot.resize.js"></script>
    <script src="/layout/plugins/count-to/jquery.countTo.js"></script>
    <!-- /page level scripts -->

    <!-- page script -->
    <script src="/layout/js/toastr-options.js"></script>
    <!-- /page script -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- template scripts -->
    <script src="/layout/js/offscreen.js"></script>
    <script src="/layout/js/main.js"></script>
    <script src="/layout/plugins/jquery.maskedinput.min.js"></script>
    <script src="/layout/plugins/bootstrap-paginator/bootstrap-paginator.min.js"></script>
    <!-- /template scripts -->

    <script src="/layout/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="/layout/plugins/toastr/toastr.js"></script>
    <script src="/layout/js/form-masks.js"></script>
    <script src="/layout/js/form-custom.js"></script>

</body>
<!-- /body -->

</html>