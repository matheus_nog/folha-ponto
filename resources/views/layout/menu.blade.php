@section('menu')
<aside class="sidebar offscreen-left">
    <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
        <p class="nav-title">MENU</p>
        <ul class="nav">

            <li>
                <a href="/dashboard">
                    <i class="ti-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li>
                <a href="/feriados/cadastro">
                    <i class="ti-home"></i>
                    <span>Feriados</span>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="toggle-accordion"></i>
                    <i class="ti-layout-media-overlay-alt-2"></i>
                    <span>Empregados</span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="/empregados">
                            <span>Listar</span>
                        </a>
                    </li>
                    <li>
                        <a href="/empregados/cadastro">
                            <span>Cadastrar</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="/gerar-folha">
                    <i class="ti-user"></i>
                    <span>Gerar folha de ponto</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>