<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empregado extends Model
{
    use HasFactory;

    protected $fillable = ['empregador', 'cpf', 'endereco', 'nome', 'ctps', 'admissao', 'funcao', 'horario_entrada_semana', 'horario_saida_almoco_semana', 'horario_retorno_almoco_semana', 'horario_saida_semana', 'horario_entrada_sabado', 'horario_saida_almoco_sabado', 'horario_retorno_almoco_sabado', 'horario_saida_sabado'];
}
