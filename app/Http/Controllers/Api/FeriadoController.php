<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Empregado;
use App\Models\Feriado;
use Illuminate\Http\Request;

class FeriadoController extends Controller
{
    public function store(Request $request)
    {
        $feriado = Feriado::query()->first();        
        if (!$feriado) {
            $feriado = new Feriado();
        }

        $feriado->nome = $request->feriados;
        $feriado->save();

        return response()->json([
            'message' => 'Feriado salvo com sucesso!',
            'data' => $feriado
        ], 200);
    }

    public function index()
    {
        $query = Feriado::query()->first();
        return $query;
    }
}
