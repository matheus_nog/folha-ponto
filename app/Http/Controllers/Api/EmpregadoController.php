<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PdfController;
use App\Models\Empregado;
use App\Models\Feriado;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class EmpregadoController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            // 'document_federalUser' => 'required|unique:users,document_federal',
            // 'document_stateUser' => 'required|unique:users,document_state',
            // 'nameUser' => 'required',
        ];

        $customMessages = [
            // 'document_federalUser.required' => 'O campo CPF(usuário) é obrigatório.',
            // 'document_federalUser.unique' => 'Esse CPF(usuário) já está cadastrado no sistema.',
            // 'document_stateUser.required' => 'O campo RG(usuário) é obrigatório.',
            // 'document_stateUser.unique' => 'Esse RG(usuário) já está cadastrado no sistema.',
            // 'nameUser.required' => 'O campo nome(usuário) é obrigatório.',
        ];

        // $this->validate($request, $rules, $customMessages);

        $empregado = new Empregado();
        $empregado->empregador = $request->empregador;
        $empregado->cpf = $request->cpf;
        $empregado->endereco = $request->endereco;
        $empregado->nome = $request->nome;
        $empregado->funcao = $request->funcao;
        $empregado->ctps = $request->ctps;
        $admissao = date('d/m/Y', strtotime($request->admissao));
        $empregado->admissao = $admissao;
        $empregado->horario_entrada_semana = $request->entrada_semana;
        $empregado->horario_saida_almoco_semana = $request->saida_almoco_semana;
        $empregado->horario_retorno_almoco_semana = $request->retorno_almoco_semana;
        $empregado->horario_saida_semana = $request->saida_semana;

        $empregado->horario_entrada_sabado = $request->entrada_sabado;
        $empregado->horario_saida_almoco_sabado = $request->saida_almoco_sabado;
        $empregado->horario_retorno_almoco_sabado = $request->retorno_almoco_sabado;
        $empregado->horario_saida_sabado = $request->saida_sabado;

        $empregado->save();

        return response()->json([
            'message' => 'Empregado criado com sucesso!',
            'data' => $empregado
        ], 200);
    }

    public function update($id, Request $request)
    {
        $empregado = Empregado::query()->find($id);
        $empregado->empregador = $request->empregador;
        $empregado->cpf = $request->cpf;
        $empregado->endereco = $request->endereco;
        $empregado->nome = $request->nome;
        $empregado->funcao = $request->funcao;
        $empregado->ctps = $request->ctps;
        $empregado->admissao = $request->admissao;
        $empregado->horario_entrada_semana = $request->entrada_semana;
        $empregado->horario_saida_almoco_semana = $request->saida_almoco_semana;
        $empregado->horario_retorno_almoco_semana = $request->retorno_almoco_semana;
        $empregado->horario_saida_semana = $request->saida_semana;

        $empregado->horario_entrada_sabado = $request->entrada_sabado;
        $empregado->horario_saida_almoco_sabado = $request->saida_almoco_sabado;
        $empregado->horario_retorno_almoco_sabado = $request->retorno_almoco_sabado;
        $empregado->horario_saida_sabado = $request->saida_sabado;

        $empregado->save();

        return response()->json([
            'message' => 'Empregado atualizado com sucesso!',
            'data' => $empregado
        ], 200);
    }

    public function index()
    {
        $query = Empregado::query();
        return $query->paginate('10')->onEachSide(1);
    }

    public function isWeekend($date)
    {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 0 || $weekDay == 6);
    }

    public function isSaturday($date)
    {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 6);
    }

    public function isSunday($date)
    {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 0);
    }

    public function isHoliday($date)
    {
        $feriados = Feriado::query()->first()->nome;
        $feriados = preg_replace("/\r|\n/", "|", $feriados);
        $feriados = explode('|', $feriados);
        // dd($date);
        if (in_array($date, $feriados)) {
            return true;
        }
        return false;
    }

    public function gerarFolha(Request $request)
    {
        if ($request->mes_ano == null) {
            return response()->json([
                'message' => 'Informe os campos corretamente!',
                'data' => ''
            ], 404);
        }

        $empregados = Empregado::all();
        $todos = array();
        foreach ($empregados as $empregado) {

            $stop = false;

            while ($stop == false) {
                $dataDividida = explode('-', $request->mes_ano);
                $data = new DateTime($dataDividida[1] . '-' . $dataDividida[0] . '-01');
                $ultimoDia = $data->format('Y-m-t');

                $begin = $data;
                $end = new DateTime($ultimoDia);

                $begin = $begin->format('d/m/Y');
                $end = $end->format('d/m/Y');

                //Star date
                $dateStart         = $begin;
                $dateStart         = implode('-', array_reverse(explode('/', substr($dateStart, 0, 10)))) . substr($dateStart, 10);
                $dateStart         = new DateTime($dateStart);

                //End date
                $dateEnd         = $end;
                $dateEnd         = implode('-', array_reverse(explode('/', substr($dateEnd, 0, 10)))) . substr($dateEnd, 10);
                $dateEnd         = new DateTime($dateEnd);

                $totalExtra = 0;
                $totalAtraso = 0;

                $dateRange = array();

                while ($dateStart <= $dateEnd) {
                    $status = rand(2, 4);

                    $extra = 0;
                    $atraso = 0;

                    $entrada_semana = new Carbon(date("H:i", strtotime($empregado->horario_entrada_semana)));
                    $saida_almoco_semana = new Carbon(date("H:i", strtotime($empregado->horario_saida_almoco_semana)));
                    $retorno_semana = new Carbon(date("H:i", strtotime($empregado->horario_retorno_almoco_semana)));
                    $saida_semana = new Carbon(date("H:i", strtotime($empregado->horario_saida_semana)));

                    $entrada_sabado = new Carbon(date("H:i", strtotime($empregado->horario_entrada_sabado)));
                    $saida_almoco_sabado = new Carbon(date("H:i", strtotime($empregado->horario_saida_almoco_sabado)));
                    $retorno_sabado = new Carbon(date("H:i", strtotime($empregado->horario_retorno_almoco_sabado)));
                    $saida_sabado = new Carbon(date("H:i", strtotime($empregado->horario_saida_sabado)));

                    $saturday = false;
                    if ($this->isSaturday($dateStart->format('Y-m-d'))) {
                        $saturday = true;
                    }

                    $sunday = false;
                    if ($this->isSunday($dateStart->format('Y-m-d'))) {
                        $sunday = true;
                    }

                    $holiday = false;
                    if ($this->isHoliday($dateStart->format('d/m/Y'))) {
                        $holiday = true;
                    }

                    // alterando entrada e saida igualmente
                    if ($status == 2) {
                        $minutes = rand(1, 5);
                        $entrada_semana->add(new DateInterval('PT' . $minutes . 'M'));
                        $saida_semana->add(new DateInterval('PT' . $minutes . 'M'));
                    }

                    if (!$saturday && !$sunday && !$holiday) {
                        // alterando horario de saida almoco / retorno almoco
                        $random = rand(1, 2);
                        if ($random == 1) {
                            // alterando para mais saida almoco
                            $minutes = rand(1, 5);
                            $saida_almoco_semana->add(new DateInterval('PT' . $minutes . 'M'));
                            $extra += $minutes;
                            $totalExtra += $extra;
                        } else if ($random == 2) {
                            // alterando para mais retorno almoco
                            $minutes = rand(1, 5);
                            $retorno_semana->add(new DateInterval('PT' . $minutes . 'M'));
                            $atraso += $minutes;
                            $totalAtraso += $atraso;
                        }
                    }

                    // alterando para mais, o horário de saida
                    if ($status == 3) {
                        if (!$sunday && !$holiday) {
                            if ($saturday) {
                                $minutes = rand(1, 5);
                                $saida_sabado->add(new DateInterval('PT' . $minutes . 'M'));
                                $extra += $minutes;
                                $totalExtra += $extra;
                            } else {
                                $minutes = rand(1, 5);
                                $saida_semana->add(new DateInterval('PT' . $minutes . 'M'));
                                $extra += $minutes;
                                $totalExtra += $extra;
                            }
                        }
                    }

                    // alterando para mais, o horário de entrada
                    if ($status == 4) {
                        if (!$sunday && !$holiday) {
                            if ($saturday) {
                                $minutes = rand(1, 5);
                                $entrada_sabado->add(new DateInterval('PT' . $minutes . 'M'));
                                $atraso += $minutes;
                                $totalAtraso += $atraso;
                            } else {
                                $minutes = rand(1, 5);
                                $entrada_semana->add(new DateInterval('PT' . $minutes . 'M'));
                                $atraso += $minutes;
                                $totalAtraso += $atraso;
                            }
                        }
                    }

                    array_push(
                        $dateRange,
                        [
                            'dia' => $dateStart->format('d/m/Y'),
                            'sabado' => $saturday,
                            'domingo' => $sunday,
                            'feriado' => $holiday,
                            'horario_entrada' => !$saturday ? $entrada_semana->format('H:i') : $entrada_sabado->format('H:i'),
                            'horario_saida_almoco' => $saida_almoco_semana->format('H:i'),
                            'horario_retorno_almoco' => $retorno_semana->format('H:i'),
                            'horario_saida' => !$saturday ? $saida_semana->format('H:i') : $saida_sabado->format('H:i'),
                            'extra' => $extra,
                            'atraso' => $atraso,
                            'total_extra' => $totalExtra,
                            'total_atraso' => $totalAtraso,
                        ]
                    );
                    $dateStart = $dateStart->modify('+1day');
                }

                if ($totalExtra > $totalAtraso) {
                    if (($totalExtra - $totalAtraso) <= 10) {
                        $stop = true;
                    }
                }
            }

            array_push(
                $todos,
                ['dias' => $dateRange, 'empregado' => $empregado, 'mes_ano' => $request->mes_ano]
            );
        }

        $pdf = new PdfController();
        $path = $pdf->gerarPdf($todos);
        array_push(
            $todos,
            ['path' => $path]
        );

        return $todos;
    }

    public function show($id)
    {
        return Empregado::find($id);
    }

    public function destroy($id)
    {
        $empregado = Empregado::query()->find($id);
        $empregado->delete();
        return response()->json([
            'message' => 'Empregado deletado com sucesso!',
            'data' => $empregado
        ], 200);
    }
}
