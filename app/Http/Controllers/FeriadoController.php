<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeriadoController extends Controller
{
    public function save($id = null)
    {
        return view('feriados.save', ['id' => $id]);
    }

    public function list()
    {
        return view('feriados.list');
    }
}
