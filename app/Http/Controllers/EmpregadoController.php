<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmpregadoController extends Controller
{
    public function save($id = null)
    {
        return view('empregados.save', ['id' => $id]);
    }

    public function list()
    {
        return view('empregados.list');
    }

    public function gerarFolha()
    {
        return view('empregados.folha');
    }
}
