<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PdfController extends Controller
{
    public function gerarPdf($todos = null)
    {
        // return \PDF::loadView('pdf.folha-ponto', compact('todos'))->download('nome-arquivo-pdf-gerado.pdf');  
        $pdf = \PDF::loadView('pdf.folha-ponto', compact('todos'));
        $fileName = (date('mdYHis') . uniqid() . '.pdf');
        $path = Storage::put('public/pdf/' . ($fileName), $pdf->output());
        return $fileName;
    }
}
