<?php

use App\Http\Controllers\EmpregadoController;
use App\Http\Controllers\FeriadoController;
use App\Http\Controllers\PdfController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('empregados/cadastro/{id?}', [EmpregadoController::class, 'save']);
Route::get('empregados/', [EmpregadoController::class, 'list']);

Route::get('feriados/cadastro/{id?}', [FeriadoController::class, 'save']);
Route::get('feriados/', [FeriadoController::class, 'list']);

Route::get('gerar-folha/', [EmpregadoController::class, 'gerarFolha']);
Route::get('pdf', [PdfController::class, 'gerarPdf'])->name('pdf');
require __DIR__.'/auth.php';
