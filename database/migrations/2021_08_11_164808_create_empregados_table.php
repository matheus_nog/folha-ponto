<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpregadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empregados', function (Blueprint $table) {
            $table->id();
            $table->string('empregador')->nullable();
            $table->string('cpf')->nullable();
            $table->string('endereco')->nullable();
            $table->string('nome')->nullable();
            $table->string('ctps')->nullable();
            $table->string('admissao')->nullable();
            $table->string('funcao')->nullable();
            
            $table->time('horario_entrada_semana')->nullable();
            $table->time('horario_saida_almoco_semana')->nullable();
            $table->time('horario_retorno_almoco_semana')->nullable();
            $table->time('horario_saida_semana')->nullable();

            $table->time('horario_entrada_sabado')->nullable();
            $table->time('horario_saida_almoco_sabado')->nullable();
            $table->time('horario_retorno_almoco_sabado')->nullable();
            $table->time('horario_saida_sabado')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empregados');
    }
}
